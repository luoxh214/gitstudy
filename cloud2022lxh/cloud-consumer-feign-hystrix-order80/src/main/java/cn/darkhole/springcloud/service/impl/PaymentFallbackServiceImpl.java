package cn.darkhole.springcloud.service.impl;

import cn.darkhole.springcloud.service.PaymentHystrixService;
import org.springframework.stereotype.Component;

/***
 cloud2022lxh
 @author luoxh
 @create 2022/2/12  18:08
 @desc
 **/
@Component
public class PaymentFallbackServiceImpl implements PaymentHystrixService {

    @Override
    public String paymentInfoOK(Integer id) {
        return "-----PaymentFallbackService fall back-paymentInfo_OK ,o(╥﹏╥)o";
    }

    @Override
    public String paymentInfoTimeOut(Integer id) {
        return "-----PaymentFallbackService fall back-paymentInfo_TimeOut ,o(╥﹏╥)o";
    }

}
