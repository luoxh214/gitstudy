package cn.darkhole.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/***
 cloud2022lxh
 @author luoxh
 @create 2022/2/11  21:11
 @desc
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class CloudProviderconsulPayment8006Application {
    public static void main(String[] args) {
        SpringApplication.run(CloudProviderconsulPayment8006Application.class, args);
        System.out.println("启动成功  CloudProviderconsulPayment8006Application ");
    }
}
