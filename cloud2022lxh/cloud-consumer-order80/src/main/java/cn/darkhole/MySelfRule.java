package cn.darkhole;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;

/***
 cloud2022lxh
 @author luoxh
 @create 2022/2/12  11:24
 @desc  IRule 的接口在ribbon中不能放在 ComponetScan扫描下扫
 **/
@Configurable
public class MySelfRule {
    @Bean
    public IRule  myRule(){
        return new RandomRule();
    }
}
