package cn.darkhole.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/***
 cloud2022lxh
 @author luoxh
 @create 2022/2/11  13:35
 @desc
 **/
@Configuration
public class ApplicationContextConfig {
      @Bean
      @LoadBalanced   //负载均衡  使用该注解赋予restTemplete负载均衡的能力
      public RestTemplate  getRestTemplate(){
          return new RestTemplate();
      }
}
