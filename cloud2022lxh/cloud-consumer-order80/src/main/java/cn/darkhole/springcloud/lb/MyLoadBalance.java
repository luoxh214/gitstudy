package cn.darkhole.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/***
 cloud2022lxh
 @author luoxh
 @create 2022/2/12  13:28
 @desc
 **/
public interface MyLoadBalance {

    ServiceInstance instances(List<ServiceInstance> serviceInstances);
}
