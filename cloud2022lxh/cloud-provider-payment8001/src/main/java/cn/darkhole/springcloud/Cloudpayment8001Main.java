package cn.darkhole.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/***
 cloud2022lxh
 @author luoxh
 @create 2022/2/10  22:50
 @desc
 **/
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class Cloudpayment8001Main {
    public static void main(String[] args) {
        SpringApplication.run(Cloudpayment8001Main.class, args);
        System.out.println("启动8001 serice  eureka");

    }
}
