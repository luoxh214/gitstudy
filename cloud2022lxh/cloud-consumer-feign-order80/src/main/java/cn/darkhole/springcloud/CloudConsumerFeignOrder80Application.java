package cn.darkhole.springcloud;

import ch.qos.logback.core.net.SyslogOutputStream;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/***
 cloud2022lxh
 @author luoxh
 @create 2022/2/12  15:14
 @desc
 **/
@SpringBootApplication
@EnableFeignClients
public class CloudConsumerFeignOrder80Application {
    public static void main(String[] args) {
        SpringApplication.run(CloudConsumerFeignOrder80Application.class,args);
        System.out.println(" CloudConsumerFeignOrder80Application  success loaded!");


    }
}
