package cn.darkhole.springcloud.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/***
 cloud2022lxh
 @author luoxh
 @create 2022/2/10  22:48
 @desc
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment implements Serializable {

   private  long id;
   private  String serial;
}
