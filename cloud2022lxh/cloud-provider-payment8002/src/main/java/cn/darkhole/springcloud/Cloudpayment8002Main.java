package cn.darkhole.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/***
 cloud2022lxh
 @author luoxh
 @create 2022/2/11  17:45
 @desc
 **/
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class Cloudpayment8002Main {
    public static void main(String[] args) {
        SpringApplication.run(Cloudpayment8002Main.class, args);
        System.out.println("启动8002 serice  eureka");

    }
}
